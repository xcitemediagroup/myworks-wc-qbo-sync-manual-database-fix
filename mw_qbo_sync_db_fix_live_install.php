<?php 
/**
 * Plugin Name:       Database Fix For MyWorks Woo Sync for QuickBooks
 * Plugin URI:        http://myworks.design/software/woocommerce-quickbooks-online-automatic-sync/
 * Description:       Verify and fixes the database errors manually if something went wrong into your server database while updating the MyWorks Woo Sync 						for QuickBooks Online plugin.
 * Version:           1.0.0
 * Author:            MyWorks Design
 * Author URI:        http://myworks.design/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */
//error_reporting(0);

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'MWQBOSYNCDBFIX_VERSION', '1.0.0' );
define( 'MWQBOSYNCDBFIX_PLUGIN', __FILE__ );
define( 'MWQBOSYNCDBFIX_PLUGIN_BASENAME', plugin_basename( MWQBOSYNCDBFIX_PLUGIN ) );
define( 'MWQBOSYNCDBFIX_PLUGIN_NAME', trim( dirname( MWQBOSYNCDBFIX_PLUGIN_BASENAME ), '/' ) );
define( 'MWQBOSYNCDBFIX_PLUGIN_DIR', untrailingslashit( dirname( MWQBOSYNCDBFIX_PLUGIN ) ) );

function mwqbosyncpo_plugin_avtivation(){

  $active_plugins = get_option('active_plugins');
  if(in_array('myworks-wc-qbo-sync/myworks-wc-qbo-sync.php',$active_plugins)){
  	return true;
  }else{
  	$error_message = __('This plugin requires <a target="_blank" href="http://myworks.design/software/woocommerce-quickbooks-online-automatic-sync/">MyWorks Woo Sync for QuickBooks Online</a> plugin to be active!', 'mw_wc_qbo_sync');
	die($error_message);
  }
}

register_activation_hook(__FILE__, 'mwqbosyncpo_plugin_avtivation');

function mwqbosyncpo_plugin_deactivation(){
	return true;
}

register_deactivation_hook( __FILE__, 'mwqbosyncpo_plugin_deactivation' );

function mwqbosyncdbfix_css(){
  wp_enqueue_style('mwqbosyncdbfix_css', plugin_dir_url( __FILE__ )."css/mwqbosyncdbfix.css");
}
add_action( 'admin_enqueue_scripts', 'mwqbosyncdbfix_css' );

function mwqbosyncdbfix_scripts(){  
  wp_enqueue_script( 'mwqbosyncdbfix_js', plugin_dir_url( __FILE__ ).'js/mwqbosyncdbfix.js', array(), '1.0.0', true );
}
add_action( 'admin_enqueue_scripts', 'mwqbosyncdbfix_scripts' );

function mwqbosyncdbfix_admin_menu(){
  	if(!class_exists('MyWorks_WC_QBO_Sync_QBO_Lib') || !class_exists('WooCommerce')) return;
	add_submenu_page( 
		'myworks-wc-qbo-sync', 
		'Database Fix',
		'Database Fix',
		'manage_options',
		'myworks-wc-qbo-sync-db-fix',
		'db_fix_live_inatall'
	);
}
add_action( 'admin_menu', 'mwqbosyncdbfix_admin_menu', 11);

function db_fix_live_inatall(){

	global $wpdb;
	global $MSQS_QL;

	$server_db = $MSQS_QL->db_check_get_fields_details();

	$tables = array(
		'mw_wc_qbo_sync_customer_pairs',
		'mw_wc_qbo_sync_log',
		'mw_wc_qbo_sync_paymentmethod_map',
		'mw_wc_qbo_sync_payment_id_map',
		'mw_wc_qbo_sync_product_pairs',
		'mw_wc_qbo_sync_promo_code_product_map',
		'mw_wc_qbo_sync_qbo_customers',
		'mw_wc_qbo_sync_qbo_items',
		'mw_wc_qbo_sync_real_time_sync_history',
		'mw_wc_qbo_sync_real_time_sync_queue',
		'mw_wc_qbo_sync_shipping_product_map',
		'mw_wc_qbo_sync_tax_map',
		'mw_wc_qbo_sync_variation_pairs',
		'mw_wc_qbo_sync_wq_cf_map'
	);

	if(isset($_GET['issue']) && $_GET['issue']!=''){
		fix_db_manually($_GET['issue']);
	}
?>
<div class="mw_qbo_sync_db_fix_body">
<div class="mw_qbo_sync_db_fix_section_page_title"><h4>Manual Database Repair</h4></div>

<?php 
foreach($tables as $table){
	if(!array_key_exists($wpdb->prefix.$table,$server_db)){
		$flag = 'error';
		?>
		<div class="mw_qbo_sync_db_fix_section">
		<p class="mw_qbo_sync_db_fix_has_error">You've missing database table <span class="mw_qbo_sync_highlight"><?php echo $wpdb->prefix.$table ?></span> on server. Please click the following link to fix.</p>
		<a class="mw_qbo_sync_db_fix_repair" href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-sync-db-fix&issue='.$table) ?>">Click here.</a>
		</div>
		<?php
	}
} 

$column_array = array(
	'mw_wc_qbo_sync_payment_id_map' => '',
	'mw_wc_qbo_sync_paymentmethod_map' => '',
	'mw_wc_qbo_sync_paymentmethod_map' => '',
	'mw_wc_qbo_sync_paymentmethod_map' => ''
);


if(!array_key_exists('is_wc_order', $server_db[$wpdb->prefix.'mw_wc_qbo_sync_payment_id_map'])){
	$flag = 'error';
?>
	<div class="mw_qbo_sync_db_fix_section">
	<p class="mw_qbo_sync_db_fix_has_error>You've missing table column <span class="mw_qbo_sync_highlight">is_wc_order</span> on <span class="mw_qbo_sync_highlight"><?php echo $wpdb->prefix.$table ?></span>. Please click the following link to fix.</p>
	<a class="mw_qbo_sync_db_fix_repair" href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-sync-db-fix&issue=is_wc_order') ?>">Click here.</a>
	</div>
	<?php
}

if(!array_key_exists('ps_order_status', $server_db[$wpdb->prefix.'mw_wc_qbo_sync_paymentmethod_map'])){
	$flag = 'error';
?>
	<div class="mw_qbo_sync_db_fix_section">
	<p class="mw_qbo_sync_db_fix_has_error>You've missing database table <span class="mw_qbo_sync_highlight"><?php echo $wpdb->prefix.$table ?></span> on server. Please click the following link to fix.</p>
	<a class="mw_qbo_sync_db_fix_repair" href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-sync-db-fix&issue=ps_order_status') ?>">Click here.</a>
	</div>
	<?php
}

if(!array_key_exists('individual_batch_support', $server_db[$wpdb->prefix.'mw_wc_qbo_sync_paymentmethod_map'])){
	$flag = 'error';
?>
	<div class="mw_qbo_sync_db_fix_section">
	<p class="mw_qbo_sync_db_fix_has_error>You've missing database table <span class="mw_qbo_sync_highlight"><?php echo $wpdb->prefix.$table ?></span> on server. Please click the following link to fix.</p>
	<a class="mw_qbo_sync_db_fix_repair" href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-sync-db-fix&issue=individual_batch_support') ?>">Click here.</a>
	</div>
	<?php
}

if(!array_key_exists('deposit_cron_utc', $server_db[$wpdb->prefix.'mw_wc_qbo_sync_paymentmethod_map'])){
	$flag = 'error';
?>
	<div class="mw_qbo_sync_db_fix_section">
	<p class="mw_qbo_sync_db_fix_has_error>You've missing database table <span class="mw_qbo_sync_highlight"><?php echo $wpdb->prefix.$table ?></span> on server. Please click the following link to fix.</p>
	<a class="mw_qbo_sync_db_fix_repair" href="<?php echo admin_url('admin.php?page=myworks-wc-qbo-sync-db-fix&issue=deposit_cron_utc') ?>">Click here.</a>
	</div>
	<?php
}

if(!isset($flag)){
?>
	<div class="mw_qbo_sync_db_fix_section">
	<p class="mw_qbo_sync_db_fix_no_error">You don't have any issue with database tables.</p>
	</div>
<?php } ?>
</div>
<?php
}

function fix_db_manually($dbfixid){

	if(isset($dbfixid) && $dbfixid!=''){

		global $wpdb;

		$dbfix = array();

		$dbfix['mw_wc_qbo_sync_variation_pairs'] = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'mw_wc_qbo_sync_variation_pairs` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `wc_variation_id` int(11) NOT NULL,
								  `quickbook_product_id` int(11) NOT NULL,
								  `class_id` varchar(255) NOT NULL,
								  PRIMARY KEY (`id`)
								) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';

		$dbfix['mw_wc_qbo_sync_wq_cf_map'] = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'mw_wc_qbo_sync_wq_cf_map` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,								  
								  `wc_field` varchar(255) NOT NULL,
								  `qb_field` varchar(255) NOT NULL,
								  PRIMARY KEY (`id`)
								) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';

		$dbfix['is_wc_order'] = "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_payment_id_map` ADD `is_wc_order` INT(1) NOT NULL DEFAULT '0' AFTER `qbo_payment_id`;";

		$dbfix['ps_order_status'] = "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` ADD `ps_order_status` VARCHAR(255) NOT NULL AFTER `term_id`;";

		$dbfix['individual_batch_support'] = "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` ADD `individual_batch_support` INT(1) NOT NULL AFTER `ps_order_status`;";

		$dbfix['deposit_cron_utc'] =  "ALTER TABLE `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` ADD `deposit_cron_utc` VARCHAR(255) NOT NULL AFTER `individual_batch_support`;";

		$wpdb->query($dbfix[$dbfixid]);

		echo $dbfix[$dbfixid];
	}
}
add_action('admin_init', 'fix_db_manually');